using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSystem : MonoBehaviour
{
    [SerializeField] BallGenerator ballGenerator = default;
    bool isDragging;
    [SerializeField] List<Ball> removeBalls = new List<Ball>();
    Ball currentDragingBall;
    void Start()
    {
        StartCoroutine(ballGenerator.Spawns(35));
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnDragStart();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            OnDragEnd();
        }
        else if (isDragging)
        {
            OnDragging();
        }
    }

    void OnDragStart()
    {
        //Debug.Log("ドラッグ開始");
        //マウスによるオブジェクトの判定
        //.Ray  マウスの位置にRayを飛ばす ぶつかったものがhitの変数の中に入る
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector2.zero);
        //hitがあるのかないのか and そのコンポーネントがBallか
        if (hit && hit.collider.GetComponent<Ball>())
        {
            Ball ball = hit.collider.GetComponent<Ball>();
            AddRemoveBall(ball);
            isDragging = true;
        }
    }

    void OnDragging()
    {
        //Debug.Log("ドラッグ中");
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector2.zero);
        if (hit && hit.collider.GetComponent<Ball>())
        {
            //同じ種類のball&距離が近かったらだったら追加
            Ball ball = hit.collider.GetComponent<Ball>();
            if (ball.id == currentDragingBall.id) //同じ種類だったら
            {
                float distance = Vector2.Distance(ball.transform.position, currentDragingBall.transform.position);
                if (distance < 1.3)//距離が近かったら
                {
                    AddRemoveBall(ball);
                }
            }

        }
    }

    void OnDragEnd()
    {
        //Debug.Log("ドラッグ終わり");
        int removeCount = removeBalls.Count;
        if (removeCount >= 3)
        {
            for (int i = 0; i < removeCount; i++)
            {
                Destroy(removeBalls[i].gameObject);  //List内を総当たり 順番にgameObjectを削除していく
            }
            StartCoroutine(ballGenerator.Spawns(removeCount+1)); //ball供給 TODOなぜかremoveCountは3以上になっているのに1個少ない数が供給される
        }

        removeBalls.Clear();  //List内の全ての要素を削除
        isDragging = false;
    }

    void AddRemoveBall(Ball ball)
    {
        currentDragingBall = ball;
        if (!removeBalls.Contains(ball))
        {
            removeBalls.Add(ball);
        }
    }
}
