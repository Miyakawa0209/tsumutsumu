using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallGenerator : MonoBehaviour
{
    [SerializeField] GameObject ballPrefab = default;
    [SerializeField] Sprite[] ballSprites = default;

    void Start()
    {
        //StartCoroutine(Spawns(30)) ;
    }

    public IEnumerator Spawns(int count)
    {
        for (int i = 1; i < count; i++)
        {
            Vector2 pos = new Vector2(Random.Range(-0.3f,0.3f),8);
            GameObject ball = Instantiate(ballPrefab,pos,Quaternion.identity);
            int ballID = Random.Range(0,ballSprites.Length);
            ball.GetComponent<SpriteRenderer>().sprite = ballSprites[ballID];
            ball.GetComponent<Ball>().id = ballID;
            yield return new WaitForSeconds(0.04f);
        }
        
    }
}
